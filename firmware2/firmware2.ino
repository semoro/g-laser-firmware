#include <simpletimer.h>
#include <math.h>
#include <SimpleList.h>
/*
#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38*/
#define X_STEP_PIN         46
#define X_DIR_PIN          48
#define X_ENABLE_PIN       62
#define X_MIN_PIN           3
#define X_MAX_PIN           2

#define Y_STEP_PIN         60
#define Y_DIR_PIN          61
#define Y_ENABLE_PIN       56
#define Y_MIN_PIN          14
#define Y_MAX_PIN          15
/*
#define Z_STEP_PIN         46
#define Z_DIR_PIN          48
#define Z_ENABLE_PIN       62*/
#define Z_MIN_PIN          18
#define Z_MAX_PIN          19

#define Z_STEP_PIN         26
#define Z_DIR_PIN          28
#define Z_ENABLE_PIN       24

#define Q_STEP_PIN         36
#define Q_DIR_PIN          34
#define Q_ENABLE_PIN       30

#define SDPOWER            -1
#define SDSS               53
#define LED_PIN            13

#define FAN_PIN            9

#define PS_ON_PIN          12
#define KILL_PIN           -1

#define HEATER_0_PIN       10
#define HEATER_1_PIN       8
#define TEMP_0_PIN          13   // ANALOG NUMBERING
#define TEMP_1_PIN          14   // ANALOG NUMBERING

#define XSTEPMM (3200/50.1)
#define ZSTEPMM (1000/15)
#define YSTEPMM (3200/49.92)

#define Minute 60.0*1000.0*1000.0


SimpleTimer timer;
SimpleList<String> args;

long int posX;
long int posY;
long int posZ;
float feed=1000;

boolean dX=false;
boolean dY=false;
boolean dZ=false;
float getArgF(char ch,float f){
  for (SimpleList<String>::iterator itr = args.begin(); itr != args.end();itr++){
    if((*itr).charAt(0)==ch){
      f=(*itr).substring(1).toFloat();
      break;
    }
  }
  Serial.print(ch);
  Serial.println(f);
  return f;
}



int xtId;
int ytId;
int ztId;
int tId;

long dlX;
long dlY;
long dlZ;


//circular

double R;
double RStep;
double aStart;
double aCurrent;
double cX;
double cY;
double aEnd;
float fX,fY;
//circular
boolean pause=false;
boolean idle=true;

void resetX(){
 digitalWrite(X_STEP_PIN,HIGH); 
}

void onTimerX(){
  posX+=dX?-1:1;
  digitalWrite(X_STEP_PIN,LOW);
  timer.setTimeout(dlX/2,&resetX);
}

void resetY(){
 digitalWrite(Y_STEP_PIN,HIGH); 
}

void onTimerY(){
  posY+=dY?-1:1;
  digitalWrite(Y_STEP_PIN,LOW);
  timer.setTimeout(dlY/2,&resetY);
}

void resetZ(){
 digitalWrite(Z_STEP_PIN,HIGH); 
}

void onTimerZ(){
  posZ+=dZ?-1:1;
  digitalWrite(Z_STEP_PIN,LOW);
  timer.setTimeout(dlZ/2,&resetZ);
}



void initJob(long int x, long int y, long int z,void (* endclb)()){
  long int sX=posX-x;
  dX=sX>0;
  long int sY=posY-y;
  dY=sY>0;
  long int sZ=posZ-z;
  dZ=sZ>0;
  
  double len=sqrt(pow(sX/XSTEPMM,2)+pow(sY/YSTEPMM,2)+pow(sZ/ZSTEPMM,2));
  long int time=(len/feed) * Minute;
  //Serial.println(String(len,DEC)+" "+String(time/abs(sX),DEC)+" "+String(time/abs(sY),DEC)+" "+String(time/abs(sZ),DEC));
  if(abs(sX)>0){
    dlX=time/abs(sX);
    xtId=timer.setTimer(dlX,&onTimerX,abs(sX));
  }
  if(abs(sY)>0){
    dlY=time/abs(sY);
    ytId=timer.setTimer(dlY,&onTimerY,abs(sY));
  }
  if(abs(sZ)>0){
    dlZ=time/abs(sZ);
    ztId=timer.setTimer(dlZ,&onTimerZ,abs(sZ));
  }
  if(time>0)
  tId=timer.setTimeout(time+max(dlX,max(dlY,dlZ))*2,endclb);
  else
    endclb();
}


void goHomeX(){
  if(digitalRead(X_MIN_PIN)==LOW){
    timer.deleteTimer(xtId);
    posX=0;
      return; 
  }
  digitalWrite(X_STEP_PIN,LOW);
  timer.setTimeout(300,&resetX);
}

void goHomeY(){
  if(digitalRead(Y_MIN_PIN)==LOW){
    timer.deleteTimer(ytId);
    posY=0;
    return; 
  }
  digitalWrite(Y_STEP_PIN,LOW);
  timer.setTimeout(300,&resetY);
}

void goHome(){
  posZ=0;
  dX=1;
  dY=1;
  xtId=timer.setInterval(600,&goHomeX);
  ytId=timer.setInterval(600,&goHomeY);
}

void sendOk(){
  Serial.println("ok."); 
}

void onDataReady(){
  int d=getArgF('D',-1);
  switch(d){
    case 0:
      timer.deleteTimer(xtId);
      timer.deleteTimer(ytId);
      timer.deleteTimer(ztId);
      return;
    case 1:
      Serial.println(String(posX/XSTEPMM ,DEC)+" "+String(posY/YSTEPMM ,DEC)+" "+String(posZ/ZSTEPMM ,DEC)+" "+String(idle));
      return;
    case 2:
      goHome();
      return;
    case 4:
      pause=true;
      timer.disable(xtId);
      timer.disable(ytId);
      timer.disable(ztId);
      return;
     case 5:
       pause=false;
       timer.enable(xtId);
       timer.enable(ytId);
       timer.enable(ztId);
       return;
  }
  int g=getArgF('G',0);
  feed=getArgF('F',feed);
  switch(g){
    case 0:
    case 1:
      {
        initJob(getArgF('X',posX/XSTEPMM)*XSTEPMM,getArgF('Y',posY/YSTEPMM)*YSTEPMM,getArgF('Z',posZ/ZSTEPMM)*ZSTEPMM,&sendOk);
      }break;
  }
}

void serial(){
  if (Serial.available()) {
      char ch=0;
      String buf="";
      args.clear();
      do{
        ch=Serial.read();
        if(ch==-1)
          continue;
        if(ch==' '){
          args.push_back(String(buf));
          buf="";
        }else if(ch!='\n'){
          buf+=ch;
        }
      }while(ch!='\n');
      args.push_back(buf);
      Serial.println("go.");
      onDataReady();
      
  }
}




void setup() {
  // put your setup code here, to run once:
  pinMode(LED_PIN  , OUTPUT);
  
  pinMode(X_STEP_PIN  , OUTPUT);
  pinMode(X_DIR_PIN    , OUTPUT);
  pinMode(X_ENABLE_PIN    , OUTPUT);
 
  
  pinMode(Y_STEP_PIN  , OUTPUT);
  pinMode(Y_DIR_PIN    , OUTPUT);
  pinMode(Y_ENABLE_PIN    , OUTPUT);
  
  pinMode(Z_STEP_PIN  , OUTPUT);
  pinMode(Z_DIR_PIN    , OUTPUT);
  pinMode(Z_ENABLE_PIN    , OUTPUT);

  pinMode(X_MIN_PIN,INPUT);
  digitalWrite(X_MIN_PIN,HIGH);
  pinMode(Y_MIN_PIN,INPUT);
  digitalWrite(Y_MIN_PIN,HIGH);
  
  Serial.begin(115200);
}





void loop() {
  
 serial();
 idle= !timer.isEnabled(xtId) || !timer.isEnabled(ytId) || !timer.isEnabled(ztId);
  // put your main code here, to run repeatedly:
  digitalWrite(X_DIR_PIN, dX?LOW:HIGH);
  digitalWrite(Y_DIR_PIN, dY?LOW:HIGH);
  digitalWrite(Z_DIR_PIN, dZ?LOW:HIGH);
  digitalWrite(X_ENABLE_PIN,idle||pause);
  digitalWrite(Y_ENABLE_PIN,idle||pause);
  digitalWrite(Z_ENABLE_PIN,idle);
  timer.run();
}
